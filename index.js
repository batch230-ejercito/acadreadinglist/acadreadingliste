console.log("The sum of numA and numB should be 5");
let numA = 2;
let numB = 3;
let sum = numA + numB;
console.log("Code Output: " + sum);
console.log(sum);

console.log("The difference of numA minus numB should be 1");
numA = 6;
numB = 5;
let difference = numA - numB;
console.log("Code Output: " + difference);
console.log(difference);

console.log("The product of numA times numB should be 8");
numA = 4;
numB = 3;
let product = numA * numB;
console.log("Code Output: " + product);
console.log(product);

console.log("The qoutient of numA divided by numB should be 3");
numA = 6;
numB = 3;
let qoutient = numA / numB;
console.log("Code Output: " + qoutient);
console.log(qoutient);
